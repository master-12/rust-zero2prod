# FROM lukemathwalker/cargo-chef:latest-rust-1.65.0 as chef
# WORKDIR /app
# RUN apt update && apt install lld clang -y

# FROM chef as planner
# COPY . .
# # Compute a lock-like file for our project
# RUN cargo chef prepare --recipe-path recipe.json

# FROM chef as builder
# COPY --from=planner /app/recipe.json recipe.json
# # Build our project dependencies, not our application!
# RUN cargo chef cook --release --recipe-path recipe.json
# # Up to this point, if our dependency tree stays the same,
# # all layers should be cached.
# COPY . .
# ENV SQLX_OFFLINE true
# # Build our project
# RUN cargo build --release --bin zero2prod

# FROM debian:stable-slim AS runtime
# WORKDIR /app
# RUN apt-get update -y \
#     && apt-get install -y --no-install-recommends openssl ca-certificates \
#  #Clean up
#     && apt-get autoremove -y \
#     && apt-get clean -y \
#     && rm -rf /var/lib/apt/lists/*

# COPY --from=builder /app/target/release/zero2prod zero2prod
# COPY configuration configuration
# #ENV APP_ENVIRONMENT production
# ENTRYPOINT ["./zero2prod"]



# #Using the `rust-musl-builder` as base image, instead of the official Rust toolchain
# FROM clux/muslrust:stable AS chef
# USER root
# RUN cargo install cargo-chef
# WORKDIR /app

# FROM chef AS planner
# COPY . .
# ENV SQLX_OFFLINE true
# RUN cargo chef prepare --recipe-path recipe.json

# FROM chef AS builder
# COPY --from=planner /app/recipe.json recipe.json
# # Notice that we are specifying the --target flag!
# RUN cargo chef cook --release --target x86_64-unknown-linux-musl --recipe-path recipe.json
# COPY . .
# ENV SQLX_OFFLINE true
# RUN cargo build --release --target x86_64-unknown-linux-musl --bin app

# FROM alpine AS runtime
# RUN addgroup -S myuser && adduser -S myuser -G myuser
# COPY --from=builder /app/target/x86_64-unknown-linux-musl/release/app /usr/local/bin/
# USER myuser
# CMD ["/usr/local/bin/app"]


# FROM rust:1.66.1 AS builder
# COPY . .
# ENV SQLX_OFFLINE true
# RUN cargo build --release --target x86_64-unknown-linux-musl --bin app

# FROM alpine AS runtime
# RUN addgroup -S myuser && adduser -S myuser -G myuser
# COPY --from=builder /app/target/x86_64-unknown-linux-musl/release/app /usr/local/bin/
# USER myuser
# CMD ["/usr/local/bin/app"]

FROM rustdocker/rust:nightly as cargo-build 
RUN apt-get update 
RUN apt-get install musl-tools -y 
RUN /root/.cargo/bin/rustup target add x86_64-unknown-linux-musl 
RUN USER=root /root/.cargo/bin/cargo new --bin zero2prod 
WORKDIR /zero2prod 
COPY ./Cargo.toml ./Cargo.toml 
COPY ./Cargo.lock ./Cargo.lock 
RUN RUSTFLAGS=-Clinker=musl-gcc /root/.cargo/bin/cargo build --release --target=x86_64-unknown-linux-musl  
RUN rm -f target/x86_64-unknown-linux-musl/release/deps/zero2prod* 
RUN rm src/*.rs 
COPY ./src ./src 
RUN RUSTFLAGS=-Clinker=musl-gcc /root/.cargo/bin/cargo build --release --target=x86_64-unknown-linux-musl  

FROM alpine:latest 
COPY --from=cargo-build /auth/target/x86_64-unknown-linux-musl/release/zero2prod . 
CMD ["./zero2prod"]